<?php

namespace Tetrapak07\ValidationAndFiltration;

use Phalcon\Di;
use Phalcon\Filter;
use stdClass;
use Supermodule\ControllerBase as SupermoduleBase;

abstract class BaseFilter
{
    protected $di;
    
    public $dataReturn;
    
    function __construct()
    {
       
        if (!SupermoduleBase::checkAndConnectModule('validfiltr')) {
            $this->dataReturn = false;  
        }  else {
           $this->dataReturn = true;
           $this->setFilter();
        }
    }


    public function initialize()
    {
       /*exit; $this->dataReturn = true;
        if (!$this->checkAndConnectModule('validfiltr')) {
            echo '!';exit;
            $this->dataReturn = false;  
        }  */
    } 
    
    protected function setFilter()
    {
        $this->di = Di::getDefault();
        $this->filter = new Filter();
       
    }
    
    public function sanitize($item, $params = [])
    {
        if (!$this->dataReturn) {
            return false;
        }
        
        # custom filters BEGIN
         $this->filter->add(
            "url",
            function ($value) {
                return filter_var($value, FILTER_VALIDATE_URL);
            }
        );
        # custom filters END
        
        return $this->filter->sanitize($item, $params);
    }
    
    public function sanitizeArray($request, $arrayKey = '', $params)
    {
        if (!$this->dataReturn) {
            return false;
        }
        $ret = new stdClass();   
        
        $items = $request->getPost($arrayKey);
        if (count($items)>0) {
            foreach ($items as $key => &$item) {
                $item = $this->filter->sanitize($item, $params);
            }
        }
        return $ret->$arrayKey = $items;
    }
    
    
   public function filter($request, $paramName = '', $params = [], $default = false) 
    {
        $ret = new stdClass();
        if (!$this->dataReturn) {
             $ret->$paramName = $request->get($paramName);
           return  $ret;
        }
      
        $ret->$paramName = $request->get($paramName, $params, $default);
        
        return $ret;
    }  

}

