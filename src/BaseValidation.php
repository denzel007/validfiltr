<?php

namespace Tetrapak07\ValidationAndFiltration;

use Phalcon\Validation;
use Supermodule\ControllerBase as SupermoduleBase;

abstract class BaseValidation extends Validation
{
    
    public function initialize()
    {
        $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('validfiltr')) {
            $this->dataReturn = false;
        }   
    } 
    
    public function show($messages)
    {
        if (!$this->dataReturn) {
            return false;
        }
        if (count($messages)) {
            foreach ($messages as $message) {
                $this->flash->error($message);   
            }
            return true;
        }
        
        return false;
    }
    
    
}