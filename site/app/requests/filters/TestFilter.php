<?php

namespace App\Requests\Filters;

class TestFilter extends \App\Requests\Filters\BaseFilter
{
    
    public function filter($request, $paramName = '', $params = [], $default = false) 
    {
        return parent::filter($request, $paramName, $params, $default);
    }    
    
} 
