<?php

namespace App\Requests\Validators;
use Phalcon\Validation\Validator\PresenceOf;

class TestValidation extends BaseValidation
{

    public function initialize()
    {
        parent::initialize();
        
         $this->add(
            "date",
            new PresenceOf(
                [
                    "message" => "The date is required",
                ]
            )
        );
        $this->setFilters("date", ["trim", "string", "striptags"]);
       
    }
}